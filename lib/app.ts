import { BotFrameworkAdapter, MemoryStorage, UserState } from 'botbuilder';
import * as restify from 'restify';
import axios from 'axios';
import Bot from './bot';

const server = restify.createServer();

server.listen(process.env.port || process.env.PORT || 3978, () => {
  // eslint-disable-next-line no-console
  console.log(`${server.name} listening on ${server.url}`);
});

const adapter = new BotFrameworkAdapter({
  appId: process.env.MICROSOFT_APP_ID,
  appPassword: process.env.MICROSOFT_APP_PASSWORD,
});

const memoryStorage = new MemoryStorage();
const userState = new UserState(memoryStorage);

const bot = new Bot(userState);

server.post('/api/messages', (req, res) => {
  adapter.processActivity(req, res, async (context) => {
    console.log(req.body.serviceUrl);
    if (req.body.isPushNotification) {
      await context.sendActivity(req.body.text);
    } else {
      await bot.run(context);
    }
  });
});

server.post('/pushNotifications', (req, res) => {
  adapter.processActivity(req, res, async () => {
    req.body.channelUserId.forEach(async (userId: string) => {
      try {
        await axios.post(`${req.body.url}/api/messages`, {
          text: req.body.text,
          type: 'message',
          channelId: req.body.channelId,
          from: {
            id: userId,
          },
          conversation: { id: req.body.conversationId[req.body.channelUserId.indexOf(userId)] },
          recipient: {
            id: req.body.recipient,
          },
          isPushNotification: true,
          serviceUrl: req.body.serviceUrl,
        });
      // eslint-disable-next-line no-console
      } catch (err) { console.error(err); }
    });
  });
});
