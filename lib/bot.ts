import { ActivityHandler, MessageFactory } from 'botbuilder';
// eslint-disable-next-line no-unused-vars
import type { TurnContext, UserState, StatePropertyAccessor } from 'botbuilder';
import roles from './roles';
import { checkUserAuthorization, getRole } from './repository';
import noneRoleHandler from './noneRole';
import studentRoleHandler, { defaultStudentCommands } from './student';
import teacherRoleHandler, { defaultTeacherCommands } from './teacher';
import administratorRoleHandler, { defaultAdministratorCommands } from './administrator';

export async function sendMessageWithCommandButtons(
  context: TurnContext,
  buttons: string[],
  message: string,
): Promise<void> {
  await context.sendActivity(MessageFactory.suggestedActions(buttons, message));
}

export async function sendSchedule(
  nextCourseModules: any[],
  context: TurnContext,
  buttons: string[],
): Promise<void> {
  if (nextCourseModules.length < 1) {
    sendMessageWithCommandButtons(context, buttons, 'Схоже що лекцій скоро не буде, спробуйте пізніше.');
    return;
  }

  const courseStrings = nextCourseModules.reduce((accumulator, courseModule) => {
    const moduleDate = new Date(courseModule.date);
    const moduleTime = new Date();
    const timesArray = (courseModule.startTime as string).split(':');

    moduleTime.setHours(Number.parseInt(timesArray[0], 10));
    moduleTime.setMinutes(Number.parseInt(timesArray[1], 10));

    return `${accumulator}${courseModule.course.name} ➡️ ${courseModule.module.name} ➡️ ${(moduleDate.getDate() < 10) ? '0' : ''}${moduleDate.getDate()}.${(moduleDate.getMonth() + 1 < 10) ? '0' : ''}${moduleDate.getMonth() + 1} ${(moduleTime.getHours() < 10) ? '0' : ''}${moduleTime.getHours()}:${(moduleTime.getMinutes() < 10) ? '0' : ''}${moduleTime.getMinutes()}\n\n`;
  }, 'Графік наступних лекцій:\n\n\n\n');

  sendMessageWithCommandButtons(context, buttons, courseStrings);
}

export default class Bot extends ActivityHandler {
    private USER_PROFILE_PROPERTY: string = 'userData';

    private userData: StatePropertyAccessor;

    private userState: UserState;

    constructor(userState: UserState) {
      super();

      this.userData = userState.createProperty(this.USER_PROFILE_PROPERTY);
      this.userState = userState;

      this.onTurn(async (context: TurnContext, next) => {
        const profile = await this.getUserProfile(context);

        if (!profile.isAuthorized) {
          if (await checkUserAuthorization(context.activity.from.id)) {
            profile.isAuthorized = true;
            profile.role = await getRole(context);
            let commands: string[];
            switch (profile.role) {
              case roles.none:
                commands = ['Авторизація'];
                break;
              case roles.student:
                commands = defaultStudentCommands;
                break;
              case roles.teacher:
                commands = defaultTeacherCommands;
                break;
              case roles.administrator:
                commands = defaultAdministratorCommands;
                break;
              default:
                commands = ['Авторизація'];
                break;
            }
            sendMessageWithCommandButtons(context, commands, 'Раді вас вітати знову! Натискайте на кнопки щоб виконувати різні команди.');
          } else if (context.activity.type === 'conversationUpdate') {
            sendMessageWithCommandButtons(context, ['Авторизація'], 'Привіт! Я Education Platform Bot! Щоб авторизуватися напиши "Авторизація"');
          }
        } else if (context.activity.type === 'conversationUpdate') {
          let commands: string[];
          switch (profile.role) {
            case roles.none:
              commands = ['Авторизація'];
              break;
            case roles.student:
              commands = defaultStudentCommands;
              break;
            case roles.teacher:
              commands = defaultTeacherCommands;
              break;
            case roles.administrator:
              commands = defaultAdministratorCommands;
              break;
            default:
              commands = ['Авторизація'];
              break;
          }
          sendMessageWithCommandButtons(context, commands, 'Раді вас вітати знову! Натискайте на кнопки щоб виконувати різні команди.');
        }

        await next();
      });

      this.onDialog(async (context, next) => {
        await this.userState.saveChanges(context, false);
        await next();
      });

      this.onMessage(async (context: TurnContext, next) => {
        const profile = await this.getUserProfile(context);

        if (profile.role === roles.none) {
          await noneRoleHandler(context, profile);
        } else if (profile.role === roles.student) {
          await studentRoleHandler(context, profile);
        } else if (profile.role === roles.teacher) {
          await teacherRoleHandler(context, profile);
        } else {
          await administratorRoleHandler(context);
        }

        await next();
      });
    }

    private async getUserProfile(context: TurnContext): Promise<any> {
      return this.userData.get(context, {
        isAuthorized: false,
        role: roles.none,
        isAutorizationCodeRequired: false,
        isWaitingForCourseName: false,
        courseId: null,
        isWaitingForModuleName: false,
        isWaitingForStudentName: false,
        chosenCourseModule: null,
        courseStudents: null,
        chosenCourseStudent: null,
        chosenStudentsName: null,
        isWaitingForMarks: false,
        isWaitingForTestMark: false,
        isWaitingForLabMark: false,
      });
    }
}
