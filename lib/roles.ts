/* eslint-disable no-unused-vars */
enum roles {
    none,
    student,
    teacher,
    administrator
}

export default roles;
