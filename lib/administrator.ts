// eslint-disable-next-line no-unused-vars
import type { TurnContext } from 'botbuilder';
import { getAllNextLectures } from './repository';
import { sendMessageWithCommandButtons, sendSchedule } from './bot';

export const defaultAdministratorCommands = ['Переглянути графік лекцій'];

// eslint-disable-next-line max-len
export default async function teacherRoleHandler(context: TurnContext): Promise<void> {
  if (context.activity.text === 'Переглянути графік лекцій') {
    const nextCourseModules = await getAllNextLectures();
    sendSchedule(nextCourseModules, context, defaultAdministratorCommands);
  } else {
    await sendMessageWithCommandButtons(context, defaultAdministratorCommands, 'Не можу знайти таку команду, спробуйте ще раз.');
  }
}
