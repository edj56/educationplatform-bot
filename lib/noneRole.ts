// eslint-disable-next-line no-unused-vars
import type { TurnContext } from 'botbuilder';
import * as repository from './repository';
import roles from './roles';
import { sendMessageWithCommandButtons } from './bot';
import { defaultStudentCommands } from './student';
import { defaultTeacherCommands } from './teacher';
import { defaultAdministratorCommands } from './administrator';

export const defaultNoneRoleCommands = ['Авторизація'];

export default async function noneRoleHandler(
  context: TurnContext,
  incomingProfile: any,
): Promise<void> {
  const profile = incomingProfile;

  if (context.activity.text === 'Авторизація') {
    await context.sendActivity('Напишіть свій код авторизації.');
    profile.isAutorizationCodeRequired = true;
  } else {
    if (profile.isAutorizationCodeRequired) {
      try {
        const responceUserId = await repository.getUserIdByKey(context);

        if (!responceUserId) {
          throw new Error();
        }

        if (!await repository.deleteAuthorizationKey(context.activity.text)) {
          throw new Error();
        }

        try {
          if (!await repository.createUser(context, responceUserId)) {
            throw new Error();
          }

          profile.isAuthorized = true;
          profile.role = await repository.getRole(context);

          if (profile.role !== roles.none) {
            let buttons = [];
            switch (profile.role) {
              case roles.student:
                buttons = defaultStudentCommands;
                break;
              case roles.teacher:
                buttons = defaultTeacherCommands;
                break;
              case roles.administrator:
                buttons = defaultAdministratorCommands;
                break;
              default:
                buttons = defaultNoneRoleCommands;
                break;
            }
            sendMessageWithCommandButtons(context, buttons, 'Ви успішно авторизувалися!');
          } else {
            sendMessageWithCommandButtons(context, defaultNoneRoleCommands, 'Щось пішло не так.');
          }
        } catch {
          sendMessageWithCommandButtons(context, defaultNoneRoleCommands, 'Ви вже маєте існуючий аккаунт.');
        }
      } catch {
        sendMessageWithCommandButtons(context, defaultNoneRoleCommands, 'Неправильний код авторизації, спробуйте ще раз!');
      }
    } else {
      sendMessageWithCommandButtons(context, defaultNoneRoleCommands, 'Не можу знайти такої команди, спробуйте ще раз.');
    }

    profile.isAutorizationCodeRequired = false;
  }
}
