// eslint-disable-next-line no-unused-vars
import type { TurnContext } from 'botbuilder';
import * as repository from './repository';
import { sendMessageWithCommandButtons, sendSchedule } from './bot';

export const defaultStudentCommands = ['Оцінка за модуль', 'Переглянути графік лекцій'];

export default async function studentRoleHandler(
  context: TurnContext,
  incomingProfile : any,
): Promise<void> {
  const profile = incomingProfile;

  if (context.activity.text === 'Оцінка за модуль') {
    const courses = await repository.getCourses(context.activity.from.id);

    if (courses.length < 1) {
      sendMessageWithCommandButtons(context, defaultStudentCommands, 'Схоже що ви не записані на жодний курс, будь-ласка, спробуйте пізніше.');
      return;
    }

    const coursesNames = [];
    for (let i = 0; i < courses.length; i += 1) {
      coursesNames.push(courses[i].name);
    }

    sendMessageWithCommandButtons(context, coursesNames, 'Напишіть назву курсу.');
    profile.isWaitingForCourseName = true;
  } else if (profile.isWaitingForCourseName) {
    const courses = await repository.getCourses(context.activity.from.id);

    const existingCourse = courses.find(
      (c) => c.name.toLowerCase() === context.activity.text.toLowerCase(),
    );

    if (existingCourse) {
      profile.courseModules = existingCourse.courseModule;
      const modulesNames = profile.courseModules.map((cm) => cm.module.name);

      sendMessageWithCommandButtons(context, modulesNames, 'Оберіть будь-ласка модуль.');
      profile.isWaitingForModuleName = true;
    } else {
      sendMessageWithCommandButtons(context, defaultStudentCommands, 'Не можу знайти цей курс, спробуйте ще раз.');
    }
    profile.isWaitingForCourseName = false;
  } else if (profile.isWaitingForModuleName) {
    const existingModule = profile.courseModules.find(
      (cm: any) => cm.module.name.toLowerCase() === context.activity.text.toLowerCase(),
    );

    if (!existingModule) {
      sendMessageWithCommandButtons(context, defaultStudentCommands, 'Не можу знайти такий модуль, спробуйте ще раз.');
      return;
    }

    const marks = await repository.getModuleMarks(
      context.activity.from.id,
      existingModule.courseModuleId,
    );

    if (marks.length < 1) {
      sendMessageWithCommandButtons(context, defaultStudentCommands, 'В цьому модулі ще нема оцінок, спробуйте пізніше.');
      return;
    }

    const message = marks.length
            && marks.reduce(
              (mess, mark) => `${mess}\nТестова - ${mark.testMark ? mark.testMark : 'не виставлено'}, Лабораторна - ${mark.labMark ? mark.labMark : 'не виставлено'}`,
              `Оцінка з модуля ${context.activity.text}`,
            );

    sendMessageWithCommandButtons(context, defaultStudentCommands, message);

    profile.isWaitingForModuleName = false;
  } else if (context.activity.text === 'Переглянути графік лекцій') {
    // eslint-disable-next-line max-len
    const nextCourseModules = await repository.getAllNextLecturesAvaliableForStudent(context.activity.from.id);
    sendSchedule(nextCourseModules, context, defaultStudentCommands);
  } else {
    sendMessageWithCommandButtons(context, defaultStudentCommands, 'Не можу знайти таку команду, спробуйте ще раз.');
  }
}
