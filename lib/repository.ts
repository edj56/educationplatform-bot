import axiosClient from 'axios';
// eslint-disable-next-line no-unused-vars
import type { TurnContext } from 'botbuilder';
import * as dotenv from 'dotenv';
import roles from './roles';

dotenv.config();

const axios = axiosClient.create({
  withCredentials: true,
  baseURL: 'http://localhost:51010/Bot' || 'https://itacademy.azurewebsites.net/Bot',
  timeout: 10 * 1000,
  headers: {
    ApiKey: process.env.API_KEY,
  },
});

export async function getUserIdByKey(context: TurnContext): Promise<any> {
  try {
    return (await axios.get(`/GetUserByKey/key=${encodeURIComponent(context.activity.text)}`)).data;
  } catch {
    return null;
  }
}

export async function createUser(context: TurnContext, responceUserId: string): Promise<boolean> {
  try {
    await axios.post(`/${context.activity.channelId}/${responceUserId}/${context.activity.from.id}/${context.activity.conversation.id}`);
    return true;
  } catch {
    return false;
  }
}

export async function getRole(context: TurnContext): Promise<roles> {
  try {
    const response = await axios.get(`/GetAuthorizedUserRole/channelUserId=${context.activity.from.id}`);

    if (response.data === 'Student') {
      return roles.student;
    } if (response.data === 'Teacher') {
      return roles.teacher;
    } if (response.data === 'Admin') {
      return roles.administrator;
    }
    return roles.none;
  } catch { return roles.none; }
}

export async function checkUserAuthorization(userId: string): Promise<boolean> {
  try {
    return (await axios.get(`/CheckBotUserAuthorization/channelUserId=${userId}`)).data;
  } catch {
    return false;
  }
}

export async function deleteAuthorizationKey(key: string): Promise<boolean> {
  try {
    await axios.post(`/DeleteAuthorizationKey/key=${encodeURIComponent(key)}`);
    return true;
  } catch {
    return false;
  }
}

export async function getCourses(channelId: string): Promise<any[]> {
  try {
    const response = await axios.get(`/GetStudentCourses/channelId=${encodeURIComponent(channelId)}`);
    return response.data;
  } catch {
    return [];
  }
}

export async function getModuleMarks(
  channelId: string,
  courseModuleId: number,
): Promise<any[]> {
  try {
    const response = await axios.get(`/GetStudentModuleMarks/channelId=${encodeURIComponent(channelId)}&courseModuleId=${encodeURIComponent(courseModuleId)}`);
    return response.data;
  } catch {
    return [];
  }
}

export async function getTeacherCourses(channelId: string): Promise<any[]> {
  try {
    const response = await axios.get(`/GetTeacherCourses/channelId=${encodeURIComponent(channelId)}`);
    return response.data;
  } catch {
    return [];
  }
}

export async function getCourseStudents(courseId: number): Promise<any[]> {
  try {
    const response = await axios.get(`/GetCourseStudents/courseId=${encodeURIComponent(courseId)}`);
    return response.data;
  } catch {
    return [];
  }
}


export async function getMark(
  courseModuleId: number,
  studentId: number,
  type: string,
): Promise<boolean | number> {
  try {
    const response = await axios.get(`/GetMark/courseModuleId=${encodeURIComponent(courseModuleId)}&studentId=${encodeURIComponent(studentId)}&type=${encodeURIComponent(type)}`);
    if (response.status !== 200) {
      return false;
    }

    if (response.data === false) {
      return false;
    }

    return response.data;
  } catch {
    return false;
  }
}


export async function placeMark(
  courseModuleId: number,
  studentId: number,
  mark: number,
  type: string,
): Promise<boolean> {
  try {
    const response = await axios.get(`/PlaceMark/courseModuleId=${encodeURIComponent(courseModuleId)}&studentId=${encodeURIComponent(studentId)}&mark=${encodeURIComponent(mark)}&type=${encodeURIComponent(type)}`);
    return response.data;
  } catch {
    return false;
  }
}

export async function getAllNextLectures(): Promise<any[]> {
  try {
    const response = await axios.get('/GetAllNextLectures/');
    return response.data;
  } catch {
    return [];
  }
}


export async function getAllNextLecturesAvaliableForStudent(channelId: string): Promise<any[]> {
  try {
    const response = await axios.get(`/GetAllNextCourseModulesAvaliableForStudent/channelId=${encodeURIComponent(channelId)}`);
    return response.data;
  } catch {
    return [];
  }
}
