// eslint-disable-next-line no-unused-vars
import type { TurnContext } from 'botbuilder';
import {
  getTeacherCourses,
  getCourseStudents,
  placeMark,
  getMark,
  getAllNextLectures,
} from './repository';
import { sendMessageWithCommandButtons, sendSchedule } from './bot';

export const defaultTeacherCommands = ['Виставити оцінку за модуль', 'Переглянути графік лекцій'];

async function sendMessageIfMarkIsPlaced(context: TurnContext, courseModuleId: number, studentId: number, type: 'Lab' | 'Test'): Promise<void> {
  const mark = await getMark(courseModuleId, studentId, type);
  if (typeof mark === 'number') {
    if (type === 'Test') {
      await context.sendActivity(`Оцінка цього студента за тест - ${mark}`);
    } else if (type === 'Lab') {
      await context.sendActivity(`Оцінка цього студента за лабораторну - ${mark}`);
    }
  }
}

// eslint-disable-next-line max-len
export default async function teacherRoleHandler(context: TurnContext, incomingProfile): Promise<void> {
  const profile = incomingProfile;

  if (context.activity.text === 'Виставити оцінку за модуль') {
    const courses = await getTeacherCourses(context.activity.from.id);

    if (courses.length < 1) {
      sendMessageWithCommandButtons(context, defaultTeacherCommands, 'Схоже що у вас ще нема жодного курсу, будь ласка, створіть курс, а потім використовуйте цю команду.');
      return;
    }

    const coursesNames = [];
    for (let i = 0; i < courses.length; i += 1) {
      coursesNames.push(courses[i].name);
    }

    sendMessageWithCommandButtons(context, coursesNames, 'Напишіть назву курсу.');
    profile.isWaitingForCourseName = true;
  } else if (profile.isWaitingForCourseName) {
    profile.isWaitingForCourseName = false;

    const courses = await getTeacherCourses(context.activity.from.id);

    const existingCourse = courses.find(
      (c) => c.name.toLowerCase() === context.activity.text.toLowerCase(),
    );

    if (existingCourse) {
      if (existingCourse.courseModule.length < 1) {
        sendMessageWithCommandButtons(context, defaultTeacherCommands, 'В цьому курсі ще нема модулів, додайте модулі і спробуйте ще раз.');
        return;
      }
      profile.courseId = existingCourse.courseId;
      profile.courseModules = existingCourse.courseModule;
      const modulesNames = profile.courseModules.map((cm: any) => cm.module.name);

      sendMessageWithCommandButtons(context, modulesNames, 'Оберіть будь-ласка модуль.');
      profile.isWaitingForModuleName = true;
    } else {
      sendMessageWithCommandButtons(context, defaultTeacherCommands, 'Не можу знайти цей курс, спробуйте ще раз.');
    }
  } else if (profile.isWaitingForModuleName) {
    profile.isWaitingForModuleName = false;

    const existingModule = profile.courseModules.find(
      (cm: any) => cm.module.name.toLowerCase() === context.activity.text.toLowerCase(),
    );

    if (existingModule) {
      if (existingModule.module.hasTest === false && existingModule.module.hasLab === false) {
        sendMessageWithCommandButtons(context, defaultTeacherCommands, 'За цей модуль не можна виставити оцінку, змініть можливі оцінки за цей модуль і спробуйте ще раз.');
      } else {
        profile.chosenCourseModule = existingModule;
        const courseStudents = await getCourseStudents(profile.courseId);
        if (courseStudents.length < 0) {
          sendMessageWithCommandButtons(context, defaultTeacherCommands, 'На цьому курсі ще нема студентів, додайте на курс студентів і спробуйте ще раз.');
        } else {
          profile.isWaitingForStudentName = true;
          profile.courseStudents = courseStudents;
          const studentNames = courseStudents.map(({ student }) => `${student.user.firstName} ${student.user.lastName} ${student.user.middleName}`);
          sendMessageWithCommandButtons(context, studentNames, 'Напишіть ім\'я студента.');
        }
      }
    } else {
      sendMessageWithCommandButtons(context, defaultTeacherCommands, 'Не можу знайти такий модуль, спробуйте ще раз.');
    }
  } else if (profile.isWaitingForStudentName) {
    profile.isWaitingForStudentName = false;

    const existingCourseStudent = profile.courseStudents.find(
      ({ student }) => `${student.user.firstName} ${student.user.lastName} ${student.user.middleName}` === context.activity.text,
    );

    if (!existingCourseStudent) {
      sendMessageWithCommandButtons(context, defaultTeacherCommands, 'Не можу знайти такого студента, спробуйте ще раз.');
    }

    profile.chosenCourseStudent = existingCourseStudent;
    profile.chosenStudentsName = context.activity.text;
    profile.isWaitingForMarks = true;
    if (profile.chosenCourseModule.module.hasTest) {
      profile.isWaitingForTestMark = true;
      await sendMessageIfMarkIsPlaced(context, profile.chosenCourseModule.courseModuleId, existingCourseStudent.studentId, 'Test');
      context.sendActivity(`Виставте оцінку за тест з цього модуля студенту ${context.activity.text}\n\nНапишіть 'Стоп' щоб відмінити виставлення оцінки\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.minTestMark}\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.maxTestMark}`);
    } else if (profile.chosenCourseModule.module.hasLab) {
      profile.isWaitingForLabMark = true;
      await sendMessageIfMarkIsPlaced(context, profile.chosenCourseModule.courseModuleId, existingCourseStudent.studentId, 'Lab');
      context.sendActivity(`Виставте оцінку за лабораторну з цього модуля студенту ${context.activity.text}\n\nНапишіть 'Стоп' щоб відмінити виставлення оцінки\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.minLabMark}\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.maxLabMark}`);
    }
  } else if (profile.isWaitingForMarks) {
    if (context.activity.text.toLowerCase() === 'стоп') {
      if (profile.isWaitingForTestMark) {
        profile.isWaitingForTestMark = false;
        if (profile.chosenCourseModule.module.hasLab) {
          profile.isWaitingForLabMark = true;
          await context.sendActivity(`Оцінка за тест студента ${profile.chosenStudentsName} не була виставлена.`);
          await sendMessageIfMarkIsPlaced(context, profile.chosenCourseModule.courseModuleId, profile.chosenCourseStudent.studentId, 'Lab');
          context.sendActivity(`Виставте оцінку за лабораторну з цього модуля студенту ${context.activity.text}\n\nНапишіть 'Стоп' щоб відмінити виставлення оцінки\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.minLabMark}\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.maxLabMark}`);
        } else {
          profile.isWaitingForMarks = false;
          sendMessageWithCommandButtons(context, defaultTeacherCommands, `Оцінка за тест студента ${profile.chosenStudentsName} не була виставлена.`);
        }
      } else if (profile.isWaitingForLabMark) {
        profile.isWaitingForLabMark = false;
        profile.isWaitingForMarks = false;
        sendMessageWithCommandButtons(context, defaultTeacherCommands, `Оцінка за лабораторну ${profile.chosenStudentsName} студента не була виставлена.`);
      }
      return;
    }

    const usersMark = Number.parseInt(context.activity.text, 10);
    if (Number.isNaN(usersMark)) {
      context.sendActivity('Не можу опрацювати це число, спробуйте ще раз.');
      return;
    }

    if (profile.isWaitingForTestMark) {
      profile.isWaitingForTestMark = false;
      // eslint-disable-next-line max-len
      if (usersMark >= profile.chosenCourseModule.module.minTestMark && usersMark <= profile.chosenCourseModule.module.maxTestMark) {
        const result = await placeMark(profile.chosenCourseModule.courseModuleId, profile.chosenCourseStudent.student.studentId, usersMark, 'Test');
        if (result === true) {
          if (profile.chosenCourseModule.module.hasLab) {
            profile.isWaitingForLabMark = true;
            await context.sendActivity(`Оцінка ${usersMark} за тест студента ${profile.chosenStudentsName} успішно виставлена!`);
            await sendMessageIfMarkIsPlaced(context, profile.chosenCourseModule.courseModuleId, profile.chosenCourseStudent.studentId, 'Lab');
            context.sendActivity(`Виставте оцінку за лабораторну з цього модуля студенту ${profile.chosenStudentsName}\n\nНапишіть 'Стоп' щоб відмінити виставлення оцінки\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.minLabMark}\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.maxLabMark}`);
          } else {
            profile.isWaitingForMarks = false;
            sendMessageWithCommandButtons(context, defaultTeacherCommands, `Оцінка ${usersMark} за тест студента ${profile.chosenStudentsName} успішно виставлена!`);
          }
        } else {
          context.sendActivity('Сталася помилка при виставленні оцінки, спробуйте пізніше (а також бажано повідомте розробникам).');
        }
      } else if (profile.chosenCourseModule.module.hasLab) {
        profile.isWaitingForLabMark = true;
        await context.sendActivity('Ця оцінка виходить за межі максимального/мінімального значення, спробуйте ще раз, або змініть максимальне/мінімальне значення оцінки в налаштуваннях модулю.');
        await sendMessageIfMarkIsPlaced(context, profile.chosenCourseModule.courseModuleId, profile.chosenCourseStudent.studentId, 'Lab');
        context.sendActivity(`Виставте оцінку за лабораторну з цього модуля студенту ${profile.chosenStudentsName}\n\nНапишіть 'Стоп' щоб відмінити виставлення оцінки\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.minLabMark}\n\nМінімальна оцінка: ${profile.chosenCourseModule.module.maxLabMark}`);
      } else {
        sendMessageWithCommandButtons(context, defaultTeacherCommands, 'Ця оцінка виходить за межі максимального/мінімального значення, спробуйте ще раз, або змініть максимальне/мінімальне значення оцінки в налаштуваннях модулю.');
      }
    } else if (profile.isWaitingForLabMark) {
      // eslint-disable-next-line max-len
      if (usersMark >= profile.chosenCourseModule.module.minLabMark && usersMark <= profile.chosenCourseModule.module.maxLabMark) {
        const result = await placeMark(profile.chosenCourseModule.courseModuleId, profile.chosenCourseStudent.student.studentId, usersMark, 'Lab');
        if (result === true) {
          profile.isWaitingForLabMark = false;
          profile.isWaitingForMarks = false;
          sendMessageWithCommandButtons(context, defaultTeacherCommands, `Оцінка ${usersMark} за лабораторну студента ${profile.chosenStudentsName} успішно виставлена!`);
        } else {
          context.sendActivity('Сталася помилка при виставленні оцінки, спробуйте пізніше (а також бажано повідомте розробникам).');
        }
      } else {
        sendMessageWithCommandButtons(context, defaultTeacherCommands, 'Ця оцінка виходить за межі максимального/мінімального значення, спробуйте ще раз, або змініть максимальне/мінімальне значення оцінки в налаштуваннях модулю.');
      }
    }
  } else if (context.activity.text === 'Переглянути графік лекцій') {
    const nextCourseModules = await getAllNextLectures();
    sendSchedule(nextCourseModules, context, defaultTeacherCommands);
  } else {
    sendMessageWithCommandButtons(context, defaultTeacherCommands, 'Не можу знайти таку команду, спробуйте ще раз.');
  }
}
